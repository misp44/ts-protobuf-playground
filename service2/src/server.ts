import { foo } from '../generated/messages';

import FooProxy from './FooProxy';
import FooService from './FooService';

function startServer(): void {
	const service = new FooService();
	const proxy = new FooProxy(service);

	const response = proxy.funnyMethod('P. Ennis');

	console.log(response)
};

startServer();
