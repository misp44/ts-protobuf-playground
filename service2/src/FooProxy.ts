import { foo } from '../generated/messages';
import IFooService from './IFooService';

export default class FooProxy {

	constructor(private service: IFooService) {}

	funnyMethod(userName: string): string {
		const response = this.service.funnyMethod(new foo.FunnyRequest({ userName }));

		return response.message;
	}
}
