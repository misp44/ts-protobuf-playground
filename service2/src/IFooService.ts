
import { foo } from '../generated/messages';

export default interface IFooService {
	funnyMethod(request: foo.FunnyRequest) : foo.FunnyResponse;
}
