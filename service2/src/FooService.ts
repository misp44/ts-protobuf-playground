import { foo } from '../generated/messages';
import IFooService from './IFooService';

export default class FooService implements IFooService {

	funnyMethod(request: foo.FunnyRequest): foo.FunnyResponse {
		const message = `Knock! Knock! - Who is there? - ${ request.userName } - Hah...`;

		return new foo.FunnyResponse({ message });
	}
}
