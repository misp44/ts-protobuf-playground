import * as $protobuf from "protobufjs";
/** Namespace foo. */
export namespace foo {

    /** Properties of a FunnyRequest. */
    interface IFunnyRequest {

        /** FunnyRequest userName */
        userName?: (string|null);
    }

    /** Represents a FunnyRequest. */
    class FunnyRequest implements IFunnyRequest {

        /**
         * Constructs a new FunnyRequest.
         * @param [properties] Properties to set
         */
        constructor(properties?: foo.IFunnyRequest);

        /** FunnyRequest userName. */
        public userName: string;

        /**
         * Creates a new FunnyRequest instance using the specified properties.
         * @param [properties] Properties to set
         * @returns FunnyRequest instance
         */
        public static create(properties?: foo.IFunnyRequest): foo.FunnyRequest;

        /**
         * Encodes the specified FunnyRequest message. Does not implicitly {@link foo.FunnyRequest.verify|verify} messages.
         * @param message FunnyRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: foo.IFunnyRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified FunnyRequest message, length delimited. Does not implicitly {@link foo.FunnyRequest.verify|verify} messages.
         * @param message FunnyRequest message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: foo.IFunnyRequest, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a FunnyRequest message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns FunnyRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): foo.FunnyRequest;

        /**
         * Decodes a FunnyRequest message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns FunnyRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): foo.FunnyRequest;

        /**
         * Verifies a FunnyRequest message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a FunnyRequest message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns FunnyRequest
         */
        public static fromObject(object: { [k: string]: any }): foo.FunnyRequest;

        /**
         * Creates a plain object from a FunnyRequest message. Also converts values to other types if specified.
         * @param message FunnyRequest
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: foo.FunnyRequest, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this FunnyRequest to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a FunnyResponse. */
    interface IFunnyResponse {

        /** FunnyResponse message */
        message?: (string|null);
    }

    /** Represents a FunnyResponse. */
    class FunnyResponse implements IFunnyResponse {

        /**
         * Constructs a new FunnyResponse.
         * @param [properties] Properties to set
         */
        constructor(properties?: foo.IFunnyResponse);

        /** FunnyResponse message. */
        public message: string;

        /**
         * Creates a new FunnyResponse instance using the specified properties.
         * @param [properties] Properties to set
         * @returns FunnyResponse instance
         */
        public static create(properties?: foo.IFunnyResponse): foo.FunnyResponse;

        /**
         * Encodes the specified FunnyResponse message. Does not implicitly {@link foo.FunnyResponse.verify|verify} messages.
         * @param message FunnyResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: foo.IFunnyResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified FunnyResponse message, length delimited. Does not implicitly {@link foo.FunnyResponse.verify|verify} messages.
         * @param message FunnyResponse message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: foo.IFunnyResponse, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a FunnyResponse message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns FunnyResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): foo.FunnyResponse;

        /**
         * Decodes a FunnyResponse message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns FunnyResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): foo.FunnyResponse;

        /**
         * Verifies a FunnyResponse message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a FunnyResponse message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns FunnyResponse
         */
        public static fromObject(object: { [k: string]: any }): foo.FunnyResponse;

        /**
         * Creates a plain object from a FunnyResponse message. Also converts values to other types if specified.
         * @param message FunnyResponse
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: foo.FunnyResponse, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this FunnyResponse to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Represents a Foo */
    class Foo extends $protobuf.rpc.Service {

        /**
         * Constructs a new Foo service.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         */
        constructor(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean);

        /**
         * Creates new Foo service using the specified rpc implementation.
         * @param rpcImpl RPC implementation
         * @param [requestDelimited=false] Whether requests are length-delimited
         * @param [responseDelimited=false] Whether responses are length-delimited
         * @returns RPC service. Useful where requests and/or responses are streamed.
         */
        public static create(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean): Foo;

        /**
         * Calls SomethingFunny.
         * @param request FunnyRequest message or plain object
         * @param callback Node-style callback called with the error, if any, and FunnyResponse
         */
        public somethingFunny(request: foo.IFunnyRequest, callback: foo.Foo.SomethingFunnyCallback): void;

        /**
         * Calls SomethingFunny.
         * @param request FunnyRequest message or plain object
         * @returns Promise
         */
        public somethingFunny(request: foo.IFunnyRequest): Promise<foo.FunnyResponse>;
    }

    namespace Foo {

        /**
         * Callback as used by {@link foo.Foo#somethingFunny}.
         * @param error Error, if any
         * @param [response] FunnyResponse
         */
        type SomethingFunnyCallback = (error: (Error|null), response?: foo.FunnyResponse) => void;
    }
}
