/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.foo = (function() {

    /**
     * Namespace foo.
     * @exports foo
     * @namespace
     */
    var foo = {};

    foo.FunnyRequest = (function() {

        /**
         * Properties of a FunnyRequest.
         * @memberof foo
         * @interface IFunnyRequest
         * @property {string|null} [userName] FunnyRequest userName
         */

        /**
         * Constructs a new FunnyRequest.
         * @memberof foo
         * @classdesc Represents a FunnyRequest.
         * @implements IFunnyRequest
         * @constructor
         * @param {foo.IFunnyRequest=} [properties] Properties to set
         */
        function FunnyRequest(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * FunnyRequest userName.
         * @member {string} userName
         * @memberof foo.FunnyRequest
         * @instance
         */
        FunnyRequest.prototype.userName = "";

        /**
         * Creates a new FunnyRequest instance using the specified properties.
         * @function create
         * @memberof foo.FunnyRequest
         * @static
         * @param {foo.IFunnyRequest=} [properties] Properties to set
         * @returns {foo.FunnyRequest} FunnyRequest instance
         */
        FunnyRequest.create = function create(properties) {
            return new FunnyRequest(properties);
        };

        /**
         * Encodes the specified FunnyRequest message. Does not implicitly {@link foo.FunnyRequest.verify|verify} messages.
         * @function encode
         * @memberof foo.FunnyRequest
         * @static
         * @param {foo.IFunnyRequest} message FunnyRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FunnyRequest.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.userName != null && message.hasOwnProperty("userName"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.userName);
            return writer;
        };

        /**
         * Encodes the specified FunnyRequest message, length delimited. Does not implicitly {@link foo.FunnyRequest.verify|verify} messages.
         * @function encodeDelimited
         * @memberof foo.FunnyRequest
         * @static
         * @param {foo.IFunnyRequest} message FunnyRequest message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FunnyRequest.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FunnyRequest message from the specified reader or buffer.
         * @function decode
         * @memberof foo.FunnyRequest
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {foo.FunnyRequest} FunnyRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FunnyRequest.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.foo.FunnyRequest();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.userName = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a FunnyRequest message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof foo.FunnyRequest
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {foo.FunnyRequest} FunnyRequest
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FunnyRequest.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FunnyRequest message.
         * @function verify
         * @memberof foo.FunnyRequest
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        FunnyRequest.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.userName != null && message.hasOwnProperty("userName"))
                if (!$util.isString(message.userName))
                    return "userName: string expected";
            return null;
        };

        /**
         * Creates a FunnyRequest message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof foo.FunnyRequest
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {foo.FunnyRequest} FunnyRequest
         */
        FunnyRequest.fromObject = function fromObject(object) {
            if (object instanceof $root.foo.FunnyRequest)
                return object;
            var message = new $root.foo.FunnyRequest();
            if (object.userName != null)
                message.userName = String(object.userName);
            return message;
        };

        /**
         * Creates a plain object from a FunnyRequest message. Also converts values to other types if specified.
         * @function toObject
         * @memberof foo.FunnyRequest
         * @static
         * @param {foo.FunnyRequest} message FunnyRequest
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FunnyRequest.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.userName = "";
            if (message.userName != null && message.hasOwnProperty("userName"))
                object.userName = message.userName;
            return object;
        };

        /**
         * Converts this FunnyRequest to JSON.
         * @function toJSON
         * @memberof foo.FunnyRequest
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        FunnyRequest.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FunnyRequest;
    })();

    foo.FunnyResponse = (function() {

        /**
         * Properties of a FunnyResponse.
         * @memberof foo
         * @interface IFunnyResponse
         * @property {string|null} [message] FunnyResponse message
         */

        /**
         * Constructs a new FunnyResponse.
         * @memberof foo
         * @classdesc Represents a FunnyResponse.
         * @implements IFunnyResponse
         * @constructor
         * @param {foo.IFunnyResponse=} [properties] Properties to set
         */
        function FunnyResponse(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * FunnyResponse message.
         * @member {string} message
         * @memberof foo.FunnyResponse
         * @instance
         */
        FunnyResponse.prototype.message = "";

        /**
         * Creates a new FunnyResponse instance using the specified properties.
         * @function create
         * @memberof foo.FunnyResponse
         * @static
         * @param {foo.IFunnyResponse=} [properties] Properties to set
         * @returns {foo.FunnyResponse} FunnyResponse instance
         */
        FunnyResponse.create = function create(properties) {
            return new FunnyResponse(properties);
        };

        /**
         * Encodes the specified FunnyResponse message. Does not implicitly {@link foo.FunnyResponse.verify|verify} messages.
         * @function encode
         * @memberof foo.FunnyResponse
         * @static
         * @param {foo.IFunnyResponse} message FunnyResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FunnyResponse.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.message != null && message.hasOwnProperty("message"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.message);
            return writer;
        };

        /**
         * Encodes the specified FunnyResponse message, length delimited. Does not implicitly {@link foo.FunnyResponse.verify|verify} messages.
         * @function encodeDelimited
         * @memberof foo.FunnyResponse
         * @static
         * @param {foo.IFunnyResponse} message FunnyResponse message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FunnyResponse.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FunnyResponse message from the specified reader or buffer.
         * @function decode
         * @memberof foo.FunnyResponse
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {foo.FunnyResponse} FunnyResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FunnyResponse.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.foo.FunnyResponse();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.message = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a FunnyResponse message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof foo.FunnyResponse
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {foo.FunnyResponse} FunnyResponse
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FunnyResponse.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FunnyResponse message.
         * @function verify
         * @memberof foo.FunnyResponse
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        FunnyResponse.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.message != null && message.hasOwnProperty("message"))
                if (!$util.isString(message.message))
                    return "message: string expected";
            return null;
        };

        /**
         * Creates a FunnyResponse message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof foo.FunnyResponse
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {foo.FunnyResponse} FunnyResponse
         */
        FunnyResponse.fromObject = function fromObject(object) {
            if (object instanceof $root.foo.FunnyResponse)
                return object;
            var message = new $root.foo.FunnyResponse();
            if (object.message != null)
                message.message = String(object.message);
            return message;
        };

        /**
         * Creates a plain object from a FunnyResponse message. Also converts values to other types if specified.
         * @function toObject
         * @memberof foo.FunnyResponse
         * @static
         * @param {foo.FunnyResponse} message FunnyResponse
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FunnyResponse.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.message = "";
            if (message.message != null && message.hasOwnProperty("message"))
                object.message = message.message;
            return object;
        };

        /**
         * Converts this FunnyResponse to JSON.
         * @function toJSON
         * @memberof foo.FunnyResponse
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        FunnyResponse.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FunnyResponse;
    })();

    foo.Foo = (function() {

        /**
         * Constructs a new Foo service.
         * @memberof foo
         * @classdesc Represents a Foo
         * @extends $protobuf.rpc.Service
         * @constructor
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         */
        function Foo(rpcImpl, requestDelimited, responseDelimited) {
            $protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
        }

        (Foo.prototype = Object.create($protobuf.rpc.Service.prototype)).constructor = Foo;

        /**
         * Creates new Foo service using the specified rpc implementation.
         * @function create
         * @memberof foo.Foo
         * @static
         * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
         * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
         * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
         * @returns {Foo} RPC service. Useful where requests and/or responses are streamed.
         */
        Foo.create = function create(rpcImpl, requestDelimited, responseDelimited) {
            return new this(rpcImpl, requestDelimited, responseDelimited);
        };

        /**
         * Callback as used by {@link foo.Foo#somethingFunny}.
         * @memberof foo.Foo
         * @typedef SomethingFunnyCallback
         * @type {function}
         * @param {Error|null} error Error, if any
         * @param {foo.FunnyResponse} [response] FunnyResponse
         */

        /**
         * Calls SomethingFunny.
         * @function somethingFunny
         * @memberof foo.Foo
         * @instance
         * @param {foo.IFunnyRequest} request FunnyRequest message or plain object
         * @param {foo.Foo.SomethingFunnyCallback} callback Node-style callback called with the error, if any, and FunnyResponse
         * @returns {undefined}
         * @variation 1
         */
        Object.defineProperty(Foo.prototype.somethingFunny = function somethingFunny(request, callback) {
            return this.rpcCall(somethingFunny, $root.foo.FunnyRequest, $root.foo.FunnyResponse, request, callback);
        }, "name", { value: "SomethingFunny" });

        /**
         * Calls SomethingFunny.
         * @function somethingFunny
         * @memberof foo.Foo
         * @instance
         * @param {foo.IFunnyRequest} request FunnyRequest message or plain object
         * @returns {Promise<foo.FunnyResponse>} Promise
         * @variation 2
         */

        return Foo;
    })();

    return foo;
})();

module.exports = $root;
