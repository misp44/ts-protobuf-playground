"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var grpc = __importStar(require("grpc"));
var FooHandler_1 = __importDefault(require("./FooHandler"));
function startServer() {
    var server = new grpc.Server();
    server.addService(FooHandler_1.default.service, FooHandler_1.default.handler);
    server.bindAsync("0.0.0.0:8080", grpc.ServerCredentials.createInsecure(), function (err, port) {
        if (err != null) {
            return console.error(err);
        }
        console.log("Server started on " + port);
    });
    server.start();
}
;
startServer();
