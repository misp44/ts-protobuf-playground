import * as grpc from 'grpc';

import FooHandler from './FooHandler';

function startServer(): void {
	const server: grpc.Server = new grpc.Server();

	server.addService(FooHandler.service, FooHandler.handler);

	server.bindAsync(
		`0.0.0.0:8080`,
		grpc.ServerCredentials.createInsecure(),
		(err: Error | null, port: number) => {
			if (err != null) {
				return console.error(err);
			}

			console.log(`Server started on ${ port }`);
		},
	);

	server.start();
};

startServer();
