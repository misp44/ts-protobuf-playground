import * as grpc from 'grpc';

import { FunnyRequest, FunnyResponse } from './proto/foo/FooService_pb';
import { FooService, IFooServer } from './proto/foo/FooService_grpc_pb';

class FooHandler implements IFooServer {
    somethingFunny = (call: grpc.ServerUnaryCall<FunnyRequest>, callback: grpc.sendUnaryData<FunnyResponse>): void => {
        const response = new FunnyResponse();

        response.setMessage(`JOKE, ${ call.request.getUsername() }`);

        callback(null, response);
    };
}

export default {
    service: FooService,
    handler: new FooHandler(),
};
