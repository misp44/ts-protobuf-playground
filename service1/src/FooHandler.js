"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FooService_pb_1 = require("./proto/foo/FooService_pb");
var FooService_grpc_pb_1 = require("./proto/foo/FooService_grpc_pb");
var FooHandler = /** @class */ (function () {
    function FooHandler() {
        this.somethingFunny = function (call, callback) {
            var response = new FooService_pb_1.FunnyResponse();
            response.setMessage("JOKE, " + call.request.getUsername());
            callback(null, response);
        };
    }
    return FooHandler;
}());
exports.default = {
    service: FooService_grpc_pb_1.FooService,
    handler: new FooHandler(),
};
