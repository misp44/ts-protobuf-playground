import { foo } from '../generated/messages';
import IFooApplication from './IFooApplication'

export default class FooApplication implements IFooApplication {

	funnyMethod(request: foo.FunnyRequest): foo.FunnyResponse {
		const message = `Knock! Knock! - Who is there? - ${ request.userName } - Hah...`;

		const jokes = [
			new foo.Joke({ text: 'fsda', rating: 2 }),
			new foo.Joke({ text: 'asd', rating: 4 }),
			new foo.Joke({ text: 'qwevcxz', rating: -2 }),
		];

		return new foo.FunnyResponse({ message, jokes });
	}
}
