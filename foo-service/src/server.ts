import FooProxy from './FooProxy';
import FooApplication from './FooApplication';
import FooInterface from './FooInterface';

function startServer(): void {
	const application = new FooApplication();
	const fooInterface = new FooInterface(application);
	const proxy = new FooProxy(fooInterface);

	const response = proxy.funnyMethod('P. Ennis');

	console.log(response);
};

startServer();
