
import { foo } from '../generated/messages';

export default interface IFooApplication {
	funnyMethod(request: foo.FunnyRequest) : foo.FunnyResponse;
}
