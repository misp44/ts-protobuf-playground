import { foo } from '../generated/messages';
import FooInterface from './FooInterface';

export default class FooProxy {

	constructor(private fooInterface: FooInterface) {}

	funnyMethod(userName: string): foo.FunnyResponse {
		const funnyRequest = new foo.FunnyRequest({ userName });
		const request = Buffer.from(foo.FunnyRequest.encode(funnyRequest).finish());

		const response = this.fooInterface.handleMethod(request);

		return foo.FunnyResponse.decode(response);
	}
}
