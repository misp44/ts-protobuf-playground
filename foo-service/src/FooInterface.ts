import { foo } from '../generated/messages';
import IFooApplication from './IFooApplication';

export default class FooInterface {

	constructor(private application: IFooApplication) {}

	handleMethod(request: Buffer): Buffer {
		const funnyRequest = foo.FunnyRequest.decode(request);

		const response = this.application.funnyMethod(funnyRequest);

		return Buffer.from(foo.FunnyResponse.encode(response).finish());
	}
}
